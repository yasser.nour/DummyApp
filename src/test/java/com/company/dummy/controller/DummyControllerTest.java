package com.company.dummy.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.company.dummy.common.VehicleStatus;
import com.company.dummy.model.Vehicle;
import com.company.dummy.service.VehicleService;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DummyControllerTest {

    @Autowired
    private MockMvc mvc;
    
    @Resource
    private VehicleService vehicleService;

    @Test
    public void getVehicleStatusTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/api/status/YS2R4X20005399401/")
        		.accept(MediaType.APPLICATION_JSON))
        		.andDo(print())
                .andExpect(status().isOk());
    }
    
    @Test
    public void updateVehicleStatusTest() throws Exception {
    	Vehicle vehicle=vehicleService.findByVehicleId("YS2R4X20005399401");
        assertEquals(VehicleStatus.CONNECTED, vehicle.getStatus());
        mvc.perform(put("/api/vehicle/YS2R4X20005399401/status/disconnected")
        		.accept(MediaType.APPLICATION_JSON))
        		.andDo(print())
                .andExpect(status().isOk());
        
        vehicle=vehicleService.findByVehicleId("YS2R4X20005399401");
        assertEquals(VehicleStatus.DISCONNECTED, vehicle.getStatus());
    }
}
