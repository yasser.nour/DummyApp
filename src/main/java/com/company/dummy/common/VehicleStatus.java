package com.company.dummy.common;

/**
 * Vehicle status values.
 */

public enum VehicleStatus {
	CONNECTED, DISCONNECTED;
}
