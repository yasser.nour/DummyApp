package com.company.dummy.service;


import com.company.dummy.model.Vehicle;

public interface VehicleService {
	
	void saveVehicle(Vehicle vehicle);
	
	Vehicle findByVehicleId(String vehicleId);

	void updateVehicle(Vehicle vehicle);

}