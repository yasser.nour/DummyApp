package com.company.dummy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.company.dummy.model.Vehicle;
import com.company.dummy.repositories.VehicleRepository;



@Service("vehicleService")
@Transactional
public class VehicleServiceImpl implements VehicleService{

	@Autowired
	VehicleRepository vehicleRepository;

	public void saveVehicle(Vehicle vehicle) {
		vehicleRepository.save(vehicle);
	}
	
	public void updateVehicle(Vehicle vehicle){
		saveVehicle(vehicle);
	}

	public Vehicle findByVehicleId(String vehicleId) {
		return vehicleRepository.findByVehicleId(vehicleId);
	}
}
