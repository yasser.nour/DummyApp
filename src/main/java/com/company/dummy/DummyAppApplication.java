package com.company.dummy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication (scanBasePackages={"com.company.dummy"})
public class DummyAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(DummyAppApplication.class, args);
	}
}
