package com.company.dummy.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.company.dummy.common.VehicleStatus;
import com.company.dummy.model.Vehicle;
import com.company.dummy.service.VehicleService;

@RestController
@RequestMapping("/api")
public class DummyController {

	public static final Logger logger = LoggerFactory.getLogger(DummyController.class);

	@Autowired
	VehicleService vehicleService; 

	// -------------------retrieve Vehicle status---------------------------------------------

	/** This method will return the vehicle status.
	 * 
	 * @return
	 */
	@RequestMapping(value = "/status/{vehicleId}", method = RequestMethod.GET)
	public ResponseEntity<String> getVehicleStatus(@PathVariable("vehicleId") String vehicleId) {
		logger.info("Fetching status for vehicle {}", vehicleId);
		Vehicle vehicle = vehicleService.findByVehicleId(vehicleId);
		if (vehicle != null && vehicle.getStatus().equals(VehicleStatus.CONNECTED)) {
			logger.info("vehicle {} is connected", vehicleId);
			return new ResponseEntity<String>("connected",HttpStatus.OK);
		}
		logger.info("vehicle {} is not connected", vehicleId);
		return new ResponseEntity<String>("disconnected", HttpStatus.NOT_FOUND);
	}
	
	// ------------------- Update a Vehicle status ------------------------------------------------

	@RequestMapping(value = "/vehicle/{vehicleId}/status/{status}", method = RequestMethod.PUT)
	public ResponseEntity<String> updateVehicle(@PathVariable("vehicleId") String vehicleId, @PathVariable("status") String status) {
		logger.info("Updating Vehicle with id {}", vehicleId);

		Vehicle vehicle = vehicleService.findByVehicleId(vehicleId);

		if (vehicle == null) {
			logger.error("Unable to update. Vehicle with id {} not found.", vehicleId);
			return new ResponseEntity<String>("Unable to upate. Vehicle with id " + vehicleId + " not found.",
					HttpStatus.NOT_FOUND);
		}
		if (status != null && status.equalsIgnoreCase("connected")) {
			vehicle.setStatus(VehicleStatus.CONNECTED);
		} else {
			vehicle.setStatus(VehicleStatus.DISCONNECTED);
		}
		

		vehicleService.updateVehicle(vehicle);
		return new ResponseEntity<String>("updated with the new status " + vehicle.getStatus(), HttpStatus.OK);
	}

}